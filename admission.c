int main()
{
    int nbEleves = 1;
    float noteEleve = 0;
    float sommeNotes = 0;
    float moyenne = 0;
    float noteMin;
    float noteMax;

    while (nbEleves < 21) {
        printf("Saisir la note d'eleve %d\n", nbEleves);
        scanf("%f", &noteEleve);
	
	#Nouvelle fonctionalité ajouté
	
        if ((noteEleve < 0) || (noteEleve > 20)) {
            printf("Note non valide. Saisissez un nombre entre 0 et 20\n");
            continue;
        }

        if (noteEleve >= 10) {
            printf("L'eleve est admis\n");
        }

        else {
            printf("L'eleve n'est pas admis\n");
        }

	if (nbEleves == 1) {
            noteMin = noteEleve;
        }

        else if (noteMin > noteEleve) {
            noteMin = noteEleve;
        }


        if (noteMax < noteEleve) {
            noteMax = noteEleve;
        }

        sommeNotes += noteEleve;
        nbEleves += 1;
    }

    moyenne = sommeNotes/nbEleves;
    printf("La moyenne de la classe est %f\n", moyenne);
    printf("Note la plus elevee: %f\n", noteMax);
    printf("Note la plus faible: %f\n", noteMin);
}
